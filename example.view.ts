namespace $.$$ {
	export class $deploy_example extends $.$deploy_example {
		info_label(): string {
			return this.name() != "" ? `=== ${this.name()}, вы задеплоили MAM-проект через GitLab CI :)` : "=== Деплой MAM-проекта через GitLab CI :)"
		}

		@ $mol_mem
		name( next?: string | undefined ): string {
			return this.$.$mol_state_local.value("name", next) ?? ""
		}
	}
}
